﻿using DALSql.Entidades;
using DALSql.Modelos;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorios
{
    public class RepositorioProductos:IRepositorio<Productos>
    {
        DB db;
        public RepositorioProductos(DB db)
        {
            this.db = db;
        }

        public string Error { get; private set; }

        public List<Productos> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from Productos"));
            }
        }

        public List<ProductosCompletosModel> TraerProductosCompletos
        {
            get
            {
                List<ProductosCompletosModel> datos = new List<ProductosCompletosModel>();
                SqlDataReader data = db.Consulta("Select * from View_ProductosCompletos");
                while (data.Read())
                {
                    datos.Add(new ProductosCompletosModel()
                    {
                        Id = data.GetInt32(0),
                        Nombre = data.GetString(1),
                        IdCategoria = data.GetInt32(2),
                        NombreCategoria = data.GetString(3),
                        Precio = data.GetFloat(4)
                    });
                }
                data.Close();
                return datos;
            }
        }

        private List<Productos> Completa(SqlDataReader sqlDataReader)
        {
            List<Productos> datos = new List<Productos>();
            while (sqlDataReader.Read())
            {
                datos.Add(new Productos()
                {
                    Id = sqlDataReader.GetInt32(0),
                    Nombre = sqlDataReader.GetString(1),
                    IdCategoria=sqlDataReader.GetInt32(2),
                    Precio=sqlDataReader.GetFloat(3)
                });
            }
            sqlDataReader.Close();
            return datos;
        }

        public Productos BuscarPorId(int id)
        {
            return Completa(db.Consulta($"Select * from Productos Where Id={id}")).SingleOrDefault();
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from Productos where Id={id}") == 1;
        }

        public Productos Insertar(Productos entidad)
        {
            string sql = $"Insert into Productos(Nombre,IdCategoria,Precio) VALUES ('{entidad.Nombre}',{entidad.IdCategoria},{entidad.Precio})";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Productos"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public Productos Modificar(Productos entidad)
        {
            string sql = $"Update Productos SET Nombre='{entidad.Nombre}', IdCategoria={entidad.IdCategoria}, Precio={entidad.Precio} where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Productos"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }
    }
}

