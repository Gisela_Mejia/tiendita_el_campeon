﻿using DALSql.Entidades;
using DALSql.Modelos;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorios
{
    public class RepositorioVentas:IRepositorio<Ventas>
    {
        DB db;
        public RepositorioVentas(DB db)
        {
            this.db = db;
        }

        public string Error { get; private set; }

        public List<Ventas> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from Ventas"));
            }
        }

        private List<Ventas> Completa(SqlDataReader sqlDataReader)
        {
            List<Ventas> datos = new List<Ventas>();
            while (sqlDataReader.Read())
            {
                datos.Add(new Ventas()
                {
                    Id = sqlDataReader.GetInt32(0),
                    Fecha=sqlDataReader.GetDateTime(1),
                    IdCliente=sqlDataReader.GetInt32(2)
                });
            }
            sqlDataReader.Close();
            return datos;
        }

        public Ventas BuscarPorId(int id)
        {
            return Completa(db.Consulta($"Select * from Ventas Where Id={id}")).SingleOrDefault();
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from Ventas where Id={id}") == 1;
        }

        public Ventas Insertar(Ventas entidad)
        {
            string sql = $"Insert into Ventas(Fecha,IdCliente) VALUES ('{entidad.Fecha.ToString("yyyy-MM-dd HH:mm:ss.fff")},{entidad.IdCliente}')";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Ventas"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public Ventas Modificar(Ventas entidad)
        {
            string sql = $"Update Ventas SET Fecha='{entidad.Fecha.ToString("yyyy-MM-dd HH:mm:ss.fff")}', IdCliente={entidad.IdCliente} where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Ventas"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public List<VentasPorClienteModel> VentasPorCliente(int IdCliente)
        {
            List<VentasPorClienteModel> datos = new List<VentasPorClienteModel>();
            SqlDataReader data = db.Consulta($"select * from View_TotalDeVentasPorCliente where IdCliente={IdCliente}");
            while (data.Read())
            {
                datos.Add(new VentasPorClienteModel()
                {
                    Id = data.GetInt32(0),
                    Fecha = data.GetDateTime(1),
                    IdCliente = data.GetInt32(2),
                    Total = data.GetFloat(3),
                    NombreCompleto = data.GetString(4)
                });
            }
            data.Close();
            return datos;
        }

        //public object ObtenerVentaCompleta(int idVenta)
        //{

        //}

    }

}
