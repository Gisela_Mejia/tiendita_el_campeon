﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorios
{
    public interface IRepositorio<T> where T : class
    {
        string Error { get; }
        T Insertar(T entidad);
        T Modificar(T entidad);
        bool Eliminar(int id);
        List<T> TraerTodos { get; }
        T BuscarPorId(int id);
    }
}
