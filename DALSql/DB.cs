﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql
{
    public class DB
    {
        SqlConnection conexion;
        string cadConexion = @"Data Source=200.79.179.167;Initial Catalog=Tiendita;Persist Security Info=True;User ID=TienditaUser;Password=Tiendita123;TrustServerCertificate=True";
        public string Error;
        public DB()
        {
            conexion = new SqlConnection(cadConexion);
            conexion.Open();
        }
        public SqlDataReader Consulta(string sql)
        {
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    Error = "";
                    Debug.WriteLine(sql);
                    SqlCommand cmd = new SqlCommand(sql, conexion);
                    SqlDataReader reader = cmd.ExecuteReader();
                    return reader;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Error = "Conexión cerrada";
                return null;
            }
        }

        public int Comando(string sql)
        {
            if(conexion.State== System.Data.ConnectionState.Open)
            {
                try
                {
                    Debug.WriteLine(sql);
                    SqlCommand cmd = new SqlCommand(sql, conexion);
                    int r=cmd.ExecuteNonQuery();
                    Error = "";
                    return r;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return -1;
                }
            }
            else
            {
                Error = "Conexión cerrada";
                return -1;
            }
        }

        public int UltimoIdInsertado(string tabla)
        {
            string sql = $"Select max(Id) from {tabla}";
            var dato = Consulta(sql);
            dato.Read();
            int max=dato.GetInt32(0);
            dato.Close();
            return max;
        }

        ~DB()
        {
            conexion.Close();
        }
    }
}
