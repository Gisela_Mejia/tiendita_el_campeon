﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Modelos
{
    public class VentasPorClienteModel
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int IdCliente { get; set; }
        public float Total { get; set; }
        public string NombreCompleto { get; set; }
    }
}
