﻿using DALSql.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql
{
    public static class FabricRepositorios
    {
        static DB Db = new DB();

        public static RepositorioClientes RepositorioClientes()
        {
            return new RepositorioClientes(Db);
        }
        public static RepositorioCategorias RepositorioCategorias() => new RepositorioCategorias(Db);
        public static RepositorioProductos RepositorioProductos() => new RepositorioProductos(Db);
        public static RepositorioVentas RepositorioVentas() => new RepositorioVentas(Db);
        public static RepositorioDetalleVenta RepositorioDetalleVenta() => new RepositorioDetalleVenta(Db);

    }
}
