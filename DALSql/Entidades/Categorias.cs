﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Entidades
{
    public class Categorias
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
